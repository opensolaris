/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * Structures and type definitions for the SMB module.
 */

#include <sys/ioccom.h>
#include <strings.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#include <smbsrv/smb_ioctl.h>

#include "smbd.h"

/*ARGSUSED*/
void *
smbd_nbt_receiver(void *arg)
{
	smb_io_t	smb_io;

	bzero(&smb_io, sizeof (smb_io));

	(void) smbd_ioctl(SMB_IOC_NBT_RECEIVE, &smb_io);
	return (NULL);
}

/*ARGSUSED*/
void *
smbd_nbt_listener(void *arg)
{
	pthread_attr_t	tattr;
	sigset_t	set;
	sigset_t	oset;
	smb_io_t	smb_io;
	pthread_t	tid;

	(void) sigfillset(&set);
	(void) sigdelset(&set, SIGTERM);
	(void) sigdelset(&set, SIGINT);
	(void) pthread_sigmask(SIG_SETMASK, &set, &oset);
	(void) pthread_attr_init(&tattr);
	(void) pthread_attr_setdetachstate(&tattr, PTHREAD_CREATE_DETACHED);

	bzero(&smb_io, sizeof (smb_io));

	while (smbd_ioctl(SMB_IOC_NBT_LISTEN, &smb_io) == 0) {
		smb_io.sio_data.error = pthread_create(&tid, &tattr,
		    smbd_nbt_receiver, NULL);
	}
	(void) pthread_attr_destroy(&tattr);

	return (NULL);
}

/*ARGSUSED*/
void *
smbd_tcp_receiver(void *arg)
{
	smb_io_t	smb_io;

	bzero(&smb_io, sizeof (smb_io));

	(void) smbd_ioctl(SMB_IOC_TCP_RECEIVE, &smb_io);
	return (NULL);
}

/*ARGSUSED*/
void *
smbd_tcp_listener(void *arg)
{
	pthread_attr_t	tattr;
	sigset_t	set;
	sigset_t	oset;
	smb_io_t	smb_io;
	pthread_t	tid;

	(void) sigfillset(&set);
	(void) sigdelset(&set, SIGTERM);
	(void) sigdelset(&set, SIGINT);
	(void) pthread_sigmask(SIG_SETMASK, &set, &oset);
	(void) pthread_attr_init(&tattr);
	(void) pthread_attr_setdetachstate(&tattr, PTHREAD_CREATE_DETACHED);

	bzero(&smb_io, sizeof (smb_io));

	while (smbd_ioctl(SMB_IOC_TCP_LISTEN, &smb_io) == 0) {
		smb_io.sio_data.error = pthread_create(&tid, &tattr,
		    smbd_tcp_receiver, NULL);
	}
	(void) pthread_attr_destroy(&tattr);

	return (NULL);
}
